import java.awt.*;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.Timer;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.*;

import com.mindfusion.common.*;
import com.mindfusion.common.Rectangle;
import com.mindfusion.drawing.*;
import com.mindfusion.drawing.awt.AwtImage;
import com.mindfusion.scheduling.*;
import com.mindfusion.scheduling.awt.*;
import com.mindfusion.scheduling.model.*;
import net.miginfocom.swing.MigLayout;


public class EventCalendar extends BasePanel {

    AwtCalendar calendar;
    Recurrence recurrence;
    ArrayList<LocalTime> notificationTimes = new ArrayList<>();
    ArrayList<String> notificationHeaders = new ArrayList<>();
    ArrayList<String> notificationDesc = new ArrayList<>();
    final String[] times = {"6:00AM", "7:00AM", "8:00AM", "9:00AM", "10:00AM", "11:00AM", "12:00PM", "1:00PM", "2:00PM", "3:00PM", "4:00PM",
            "5:00PM", "6:00PM", "7:00PM", "8:00PM", "9:00PM", "10:00PM", "11:00PM"};
    HashMap<String, LocalTime> timesMap = new HashMap<>();

    public EventCalendar(JPanel p, String title) {
        super(p, title);

        LocalTime sixAM = LocalTime.of(6, 0, 0, 0);
        LocalTime sevenAM = LocalTime.of(7, 0, 0, 0);
        LocalTime eightAM = LocalTime.of(8, 0, 0, 0);
        LocalTime nineAM = LocalTime.of(9, 0, 0, 0);
        LocalTime tenAM = LocalTime.of(10, 0, 0, 0);
        LocalTime elevenAM = LocalTime.of(11, 0, 0, 0);
        LocalTime twelvePM = LocalTime.of(12, 0, 0, 0);
        LocalTime onePM = LocalTime.of(13, 0, 0, 0);
        LocalTime twoPM = LocalTime.of(14, 0, 0, 0);
        LocalTime threePM = LocalTime.of(15, 0, 0, 0);
        LocalTime fourPM = LocalTime.of(16, 0, 0, 0);
        LocalTime fivePM = LocalTime.of(17, 0, 0, 0);
        LocalTime sixPM = LocalTime.of(18, 0, 0, 0);
        LocalTime sevenPM = LocalTime.of(19, 0, 0, 0);
        LocalTime eightPM = LocalTime.of(20, 0, 0, 0);
        LocalTime ninePM = LocalTime.of(21, 0, 0, 0);
        LocalTime tenPM = LocalTime.of(22, 0, 0, 0);
        LocalTime elevenPM = LocalTime.of(23, 0, 0, 0);
        timesMap.put("6:00AM", sixAM);
        timesMap.put("7:00AM",sevenAM);
        timesMap.put("8:00AM",eightAM);
        timesMap.put("9:00AM",nineAM);
        timesMap.put("10:00AM",tenAM);
        timesMap.put("11:00AM",elevenAM);
        timesMap.put("12:00PM",twelvePM);
        timesMap.put("1:00PM",onePM);
        timesMap.put("2:00PM",twoPM);
        timesMap.put("3:00PM",threePM);
        timesMap.put("4:00PM",fourPM);
        timesMap.put("5:00PM",fivePM);
        timesMap.put("6:00PM",sixPM);
        timesMap.put("7:00PM",sevenPM);
        timesMap.put("8:00PM",eightPM);
        timesMap.put("9:00PM",ninePM);
        timesMap.put("10:00PM",tenPM);
        timesMap.put("11:00PM",elevenPM);

        BorderLayout layout = new BorderLayout();
        p.setLayout(layout);

        JLabel label = new JLabel("Set up your drug reminders by clicking on a date!");
        label.setBorder(new EmptyBorder(10, 10, 10, 10));
        p.add(label, BorderLayout.NORTH);

        // Calendar initialization start
        calendar = new AwtCalendar();
        calendar.beginInit();
        //set the current time
        calendar.setCurrentTime(DateTime.now());
        DateTime today = DateTime.today();
        //set the current date
        calendar.setDate(today);
        // Select the current date
        calendar.getSelection().set(DateTime.today());

        calendar.setCurrentView(CalendarView.SingleMonth);
        calendar.setCustomDraw(CustomDrawElements.CalendarItem);
        calendar.getMonthSettings().getDaySettings().setHeaderSize(20);
        calendar.getItemSettings().setSize(32);
        calendar.endInit();


        //add a listener for custom draw
        calendar.addCalendarListener(new CalendarAdapter() {
            @Override()
            public void draw(DrawEvent e) {
                onDraw(e);
            }
        });

        //add a listener for selecting events
        calendar.addCalendarListener(new CalendarAdapter() {
            public void dateClick(ResourceDateEvent e) {
                onDateClicked(e);
            }

        });
        calendar.addCalendarListener(new CalendarAdapter() {
            public void itemClick(ItemMouseEvent e) {
                onEventClicked(e);
            }

        });
        calendar.setSize(400, 600);

        // arrange the calendar
        p.add(calendar);
        pack();
        p.setVisible(true);

        JFrame notificationFrame = new JFrame("Event Reminder");
        notificationFrame.setLayout(new MigLayout());
        JLabel reminderLabel = new JLabel();
        reminderLabel.setFont(reminderLabel.getFont ().deriveFont (20.0f));
        reminderLabel.setForeground(Color.RED);
        JLabel descLabel = new JLabel();
        descLabel.setFont(reminderLabel.getFont ().deriveFont (20.0f));
        descLabel.setForeground(Color.RED);
        notificationFrame.setSize(400,300);
        notificationFrame.add(reminderLabel, "span, grow");
        notificationFrame.add(descLabel, "span, grow");

        Timer timer = new Timer();
        long interval = (100); // 1 sec

        timer.schedule(new TimerTask() {
            public void run() {
                for (int i = 0; i < notificationTimes.size(); i++) {
                    if (notificationTimes.get(i).until(LocalTime.now(), ChronoUnit.NANOS) > 0 &&
                            notificationTimes.get(i).until(LocalTime.now(), ChronoUnit.NANOS) < 200000000) {
                        reminderLabel.setText("EVENT REMINDER: " + notificationHeaders.get(i));
                        descLabel.setText("DESCRIPTION: " + notificationDesc.get(i));
                        notificationFrame.setVisible(true);
                    }
                }
            }
        }, 0, interval);
    }


    /* called when a date is clicked. Creates a recurring appointment. */
    protected void onDateClicked(ResourceDateEvent e) {
            /*
            int dayIndex = e.getDate().getDayOfWeek();

            Appointment item = new Appointment();
            item.setStartTime(e.getDate());
            item.setEndTime(e.getDate());
            item.setHeaderText(events[dayIndex]);
            item.getStyle().setBrush(brushes[dayIndex]);

            // Create the recurrence pattern.
            recurrence = new Recurrence();
            recurrence.setPattern(RecurrencePattern.Weekly);
            recurrence.setDaysOfWeek(getDayOfWeek(dayIndex));
            recurrence.setStartDate(e.getDate());
            recurrence.setRecurrenceEnd(RecurrenceEnd.Never);
            item.setRecurrence(recurrence);

            calendar.getSchedule().getItems().add(item);
            */
        JFrame newEventFrame = new JFrame("Schedule Event");
        newEventFrame.setLayout(new MigLayout());
        newEventFrame.setSize(new Dimension(400, 500));
        DateTime currentDate = e.getDate();
        int dayIndex = e.getDate().getDayOfWeek();

        JLabel eventNameLabel = new JLabel();
        JLabel alertTimeLabel = new JLabel();
        JLabel descriptionLabel = new JLabel();
        eventNameLabel.setText("Event Name");
        alertTimeLabel.setText("Alert Time");
        descriptionLabel.setText("Event Description");
        JTextField eventNameField = new JTextField();
        JComboBox alertTimeBox = new JComboBox(times);
        eventNameField.setColumns(30);
        JTextArea descriptionArea = new JTextArea(10, 30);
        JCheckBox weeklyCheckBox = new JCheckBox("Weekly");
        JCheckBox dailyCheckBox = new JCheckBox("Daily");
        JButton scheduleEventButton = new JButton("Schedule Event");
        scheduleEventButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e1) {
                Appointment item = new Appointment();
                item.setHeaderText(eventNameField.getText() + " - " + alertTimeBox.getSelectedItem());
                item.setDescriptionText(descriptionArea.getText());
                item.setStartTime(currentDate);
                item.setEndTime(currentDate);
                item.getStyle().setBrush(brushes[dayIndex]);

                if (dailyCheckBox.isSelected() && weeklyCheckBox.isSelected()) {
                    JFrame warningFrame = new JFrame("Error");
                    warningFrame.setSize(370, 100);
                    JLabel warningLabel = new JLabel("  You selected both weekly and daily. Please select only one.");
                    warningFrame.add(warningLabel);
                    warningFrame.setVisible(true);
                    return;
                }

                else if (weeklyCheckBox.isSelected()) {
                    recurrence = new Recurrence();
                    recurrence.setPattern(RecurrencePattern.Weekly);
                    recurrence.setDaysOfWeek(getDayOfWeek(dayIndex));
                    recurrence.setStartDate(e.getDate());
                    recurrence.setRecurrenceEnd(RecurrenceEnd.Never);
                    item.setRecurrence(recurrence);
                }

                else if (dailyCheckBox.isSelected()) {
                    recurrence = new Recurrence();
                    recurrence.setPattern(RecurrencePattern.Daily);
                    recurrence.setDaysOfWeek(getDayOfWeek(dayIndex));
                    recurrence.setStartDate(e.getDate());
                    recurrence.setRecurrenceEnd(RecurrenceEnd.Never);
                    item.setRecurrence(recurrence);
                    notificationTimes.add(timesMap.get(alertTimeBox.getSelectedItem()));
                    notificationHeaders.add(eventNameField.getText());
                    notificationDesc.add(descriptionArea.getText());
                }

                calendar.getSchedule().getItems().add(item);
                calendar.setAllowInplaceEdit(false);
                newEventFrame.dispose();
            }
        });
        newEventFrame.add(eventNameLabel);
        newEventFrame.add(eventNameField, "wrap");
        newEventFrame.add(alertTimeLabel);
        newEventFrame.add(alertTimeBox, "wrap");
        newEventFrame.add(descriptionLabel);
        newEventFrame.add(descriptionArea, "wrap");
        newEventFrame.add(weeklyCheckBox);
        newEventFrame.add(dailyCheckBox, "wrap");
        newEventFrame.add(scheduleEventButton);
        newEventFrame.setVisible(true);
    }

    protected void onEventClicked(ItemMouseEvent e) {
        JFrame viewEventFrame = new JFrame("Event Details");
        viewEventFrame.setLayout(new MigLayout());
        viewEventFrame.setSize(new Dimension(400, 500));

        JLabel eventNameLabel = new JLabel();
        JLabel alertTimeLabel = new JLabel();
        JLabel descriptionLabel = new JLabel();
        eventNameLabel.setText("Event Name");
        alertTimeLabel.setText("Alert Time");
        descriptionLabel.setText("Event Description");

        String[] array = e.getItem().getHeaderText().split(" ");
        String time = array[array.length - 1];
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < array.length - 2; i++) {
            builder.append(array[i]).append(" ");
        }
        String eventName = builder.toString();

        JTextField eventNameField = new JTextField();
        eventNameField.setText(eventName);
        eventNameField.setColumns(30);
        eventNameField.setEditable(false);
        JTextField alertTimeField = new JTextField();
        alertTimeField.setColumns(30);
        alertTimeField.setText(time);
        alertTimeField.setEditable(false);
        JTextArea descriptionArea = new JTextArea(10, 30);
        descriptionArea.setText(e.getItem().getDescriptionText());
        descriptionArea.setLineWrap(true);
        descriptionArea.setWrapStyleWord(true);
        descriptionArea.setEditable(false);

        JButton deleteButton = new JButton("Delete Event");
        deleteButton.setBackground(Color.RED);
        deleteButton.setOpaque(true);
        deleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e1) {
                if (e.getItem().getRecurrence() != null) {
                    Item master = e.getItem().getRecurrence().getMaster();
                    master.setRecurrence(null);
                    calendar.getSchedule().getItems().remove(master);
                    calendar.repaint();
                    viewEventFrame.dispose();
                }
                else {
                    calendar.getSchedule().getItems().remove(e.getItem());
                    calendar.repaint();
                    viewEventFrame.dispose();
                }
            }
        });

        viewEventFrame.add(eventNameLabel);
        viewEventFrame.add(eventNameField, "wrap");
        viewEventFrame.add(alertTimeLabel);
        viewEventFrame.add(alertTimeField, "wrap");
        viewEventFrame.add(descriptionLabel);
        viewEventFrame.add(descriptionArea, "wrap");
        viewEventFrame.add(deleteButton, "span, grow");
        viewEventFrame.setVisible(true);
    }

    /* custom drawing is performed here/ */
    private void onDraw(DrawEvent e) {
        if (recurrence == null)
            return;
        if (e.getElement() == CustomDrawElements.CalendarItem) {
            if (e.getDate().getDay() == 6) {
                java.awt.Image img = null;

                try {
                    // Read the image file from an input stream
                    InputStream is = new BufferedInputStream(
                            new FileInputStream("../cake.png"));
                    img = ImageIO.read(is);

                } catch (IOException ioe) {
                }

                //gets the bounds of the drawing area
                Rectangle r = e.getBounds();
                AwtImage awtImage = new AwtImage(img);
                //draw the image
                e.getGraphics().drawImage(awtImage, e.getBounds().getLeft(), e.getBounds().getTop(), 32, 32);

            }
        }
    }

    /* gets the day of the week as a value of the DaysOfWeek enumeration.
    The arguments identifies the day of the week as an integer. */
    private int getDayOfWeek(int i) {

        switch (i) {
            case 1:
                return DaysOfWeek.Monday;
            case 2:
                return DaysOfWeek.Tuesday;
            case 3:
                return DaysOfWeek.Wednesday;
            case 4:
                return DaysOfWeek.Thursday;
            case 5:
                return DaysOfWeek.Friday;
            case 6:
                return DaysOfWeek.Saturday;
        }
        return DaysOfWeek.Sunday;
    }

    Brush[] brushes = {
            Brushes.AliceBlue, Brushes.Beige, Brushes.LightBlue,
            Brushes.LightGreen, Brushes.LightGray, Brushes.LightPink,
            Brushes.LemonChiffon
    };

}

