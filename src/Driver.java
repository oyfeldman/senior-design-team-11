import javax.swing.*;
import java.awt.*;
import java.awt.Graphics;
import javax.swing.JFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Driver {

    //main method
    public static  void main(String[] args){

        JFrame frame = new JFrame();
        JTabbedPane tp;
        JPanel welcome, profile, home, warnings, results, emergency, news, calendar;
        frame.setSize(1200,850);
        frame.setLocationRelativeTo(null);

        welcome = new JPanel();
        profile = new JPanel();
        home = new JPanel();
        warnings = new JPanel();
        // results = new JPanel();
        emergency = new JPanel();
        // news = new JPanel();
        calendar = new JPanel();

        tp = new JTabbedPane();

        tp.setBounds(20,20,700,700);
        Welcome w1  = new Welcome(welcome,"Welcome");
        tp.add(w1.getTitle(),w1.getP());
        Profile p1 = new Profile(profile,"Profile", w1.getFirst(), w1.getMiddle(), w1.getLast(), w1.getMonth(),w1.getDay(),w1.getYear(),w1.getAge(),w1.getHeight(),w1.getWeight());
        tp.add(p1.getTitle(), p1.getP());
        Home h1 = new Home(home, "Tutorial");
        tp.add(h1.getTitle(), h1.getP());
        Warnings w = new Warnings(warnings, "Warnings");
        tp.add(w.getTitle(), w.getP());
        // tp.add("Results", results);
        Emergency e1 = new Emergency(emergency, "Emergency");
        tp.add(e1.getTitle(), e1.getP());
        // tp.add("New", news);
        EventCalendar c1 = new EventCalendar(calendar, "Calendar");
        tp.add(c1.getTitle(), c1.getP());

        frame.add(tp);

        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        frame.setVisible(true);



    }
}
