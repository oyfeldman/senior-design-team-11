import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DrugInformation {
    private final String[] names;
    String[] descriptions;
    String[][] conditions;
    List<HashMap<String, String>> interactions = new ArrayList<>();

    DrugInformation() {
        names = new String[]{"Morphine", "Acetaminophen", "Codeine", "Ibuprofen", "Salbutamol"};
        descriptions = new String[]{"Morphine, the main alkaloid of opium, was first obtained from poppy seeds" +
                " in 1805. It is a potent analgesic, though its use is limited due to tolerance, withdrawal, and the " +
                "risk of abuse. Morphine is still routinely used today, though there are a number of semi-synthetic" +
                " opioids of varying strength such as codeine, fentanyl, methadone, hydrocodone, hydromorphone, " +
                "meperidine, and oxycodone.",
                "Acetaminophen (paracetamol), also commonly known as Tylenol, is the most commonly taken analgesic" +
                        " worldwide and is recommended as first-line therapy in pain conditions by the World Health" +
                        " Organization (WHO). It is also used for its antipyretic effects, helping to reduce fever." +
                        " This drug was initially approved by the U.S. FDA in 1951 and is available in a variety of " +
                        "forms including syrup form, regular tablets, effervescent tablets, injection, suppository, " +
                        "and other forms.\n" + "Acetaminophen is often found combined with other drugs in" +
                        " more than 600 over the counter (OTC) allergy medications, cold medications, sleep medications," +
                        " pain relievers, and other products. Confusion about dosing of this drug may be caused by the" +
                        " availability of different formulas, strengths, and dosage instructions for children of" +
                        " different ages. Due to the possibility of fatal overdose and liver failure associated with " +
                        "the incorrect use of acetaminophen, it is important to follow current and available national" +
                        " and manufacturer dosing guidelines while this drug is taken or prescribed.",
                "The relief of pain (analgesia) is a primary goal for enhancing the quality of life of patients and for" +
                        " increasing the ability of patients to engage in day to day activities. Codeine, an opioid analgesic," +
                        " was originally approved in the US in 1950 and is a drug used to decrease pain by increasing the" +
                        " threshold for pain without impairing consciousness or altering other sensory functions. Opiates such" +
                        " as codeine are derived from the poppy plant, Papaver somniferum (Papaveraceae).\n" + "Codeine is" +
                        " utilized as a central analgesic, sedative, hypnotic, antinociceptive, and antiperistaltic agent, and " +
                        "is also recommended in certain diseases with incessant coughing.",
                "Ibuprofen is a non-steroidal anti-inflammatory drug (NSAID) derived from propionic acid and it is considered" +
                        " the first of the propionics. The formula of ibuprofen is 2-(4-isobutylphenyl) propionic acid and its" +
                        " initial development was in 1960 while researching for a safer alternative for aspirin. Ibuprofen was" +
                        " finally patented in 1961 and this drug was first launched against rheumatoid arthritis in the UK in" +
                        " 1969 and USA in 1974. It was the first available over-the-counter NSAID.\n" + "On the available" +
                        " products, ibuprofen is administered as a racemic mixture. Once administered, the R-enantiomer undergoes" +
                        " extensive interconversion to the S-enantiomer in vivo by the activity of the alpha-methylacyl-CoA" +
                        " racemase. In particular, it is generally proposed that the S-enantiomer is capable of eliciting" +
                        " stronger pharmacological activity than the R-enantiomer.",
                "Salbutamol is a short-acting, selective beta2-adrenergic receptor agonist used in the treatment of asthma and" +
                        " COPD. It is 29 times more selective for beta2 receptors than beta1 receptors giving it higher" +
                        " specificity for pulmonary beta receptors versus beta1-adrenergic receptors located in the heart." +
                        " Salbutamol is formulated as a racemic mixture of the R- and S-isomers. The R-isomer has 150 times" +
                        " greater affinity for the beta2-receptor than the S-isomer and the S-isomer has been associated with" +
                        " toxicity. This lead to the development of levalbuterol, the single R-isomer of salbutamol. However," +
                        " the high cost of levalbuterol compared to salbutamol has deterred wide-spread use of this" +
                        " enantiomerically pure version of the drug. Salbutamol is generally used for acute episodes of" +
                        " bronchospasm caused by bronchial asthma, chronic bronchitis and other chronic bronchopulmonary" +
                        " disorders such as chronic obstructive pulmonary disorder (COPD). It is also used prophylactically " +
                        "for exercise-induced asthma."};
        String[] array1 = new String[]{"Pain, Chronic", "Severe Pain"};
        String[] array2 = new String[]{"Allergies", "Coughing", "Common Cold/Flu", "Headache", "Muscle Injuries"};
        String[] array3 = new String[]{"Common Cold", "Coughing", "Pain", "Upper respiratory symptoms", "Flu caused by Influenza"};
        String[] array4 = new String[]{"Fever", "Sinus Pressure", "Migraine", "Menstrual Cramps", "Osteoarthritis"};
        String[] array5 = new String[]{"Asthma", "Chronic Bronchitis", "Emphysema"};
        conditions = new String[][] { array1, array2, array3, array4, array5 };

        HashMap<String, String> morphineInteractions = new HashMap<>();
        morphineInteractions.put("Doxylamine", "Doxylamine may increase the central nervous system depressant (CNS depressant) activities of Morphine.");
        morphineInteractions.put("Regorafenib", "The metabolism of Morphine can be decreased when combined with Regorafenib.");
        morphineInteractions.put("Midazolam", "The risk or severity of adverse effects can be increased when Morphine is combined with Midazolam.");
        interactions.add(morphineInteractions);

        HashMap<String, String> acetaminophenInteractions = new HashMap<>();
        acetaminophenInteractions.put("Clorindione", "Acetaminophen may increase the anticoagulant activities of Clorindione.");
        acetaminophenInteractions.put("Meloxicam", "The risk or severity of adverse effects can be increased when Acetaminophen is combined with Meloxicam.");
        acetaminophenInteractions.put("Tretinoin", "The metabolism of Tretinoin can be decreased when combined with Acetaminophen.");
        interactions.add(acetaminophenInteractions);

        HashMap<String, String> codeineInteractions = new HashMap<>();
        codeineInteractions.put("Acetophenazine", "The risk or severity of hypotension and CNS depression can be increased when Acetophenazine is combined with Codeine.");
        codeineInteractions.put("Glycerin", "The therapeutic efficacy of Glycerin can be decreased when used in combination with Codeine.");
        codeineInteractions.put("Nicotine", "The risk or severity of adverse effects can be increased when Nicotine is combined with Codeine.");
        interactions.add(codeineInteractions);

        HashMap<String, String> ibuprofenInteractions = new HashMap<>();
        ibuprofenInteractions.put("Abacavir", "Ibuprofen may decrease the excretion rate of Abacavir which could result in a higher serum level.");
        ibuprofenInteractions.put("Abametapir", "The serum concentration of Ibuprofen can be increased when it is combined with Abametapir.");
        ibuprofenInteractions.put("Abatacept", "The metabolism of Ibuprofen can be increased when combined with Abatacept.");
        interactions.add(ibuprofenInteractions);

        HashMap<String, String> salbutamolInteractions = new HashMap<>();
        salbutamolInteractions.put("Ephedrine", "The risk or severity of hypertension can be increased when Salbutamol is combined with Ephedrine.");
        salbutamolInteractions.put("Relugolix", "The risk or severity of QTc prolongation can be increased when Salbutamol is combined with Relugolix.");
        salbutamolInteractions.put("Zolmitriptan", "The risk or severity of hypertension can be increased when Zolmitriptan is combined with Salbutamol.");
        interactions.add(salbutamolInteractions);
    }

    public String findMatches(String text) {
        String drugName = "";
        for (int i = 0; i < 5; i++) {
            if (names[i].toLowerCase().equals(text.toLowerCase())) {
                drugName = names[i];
            }
        }
        return drugName;
    }

    public String getDescription(String text) {
        int index = 0;
        for (int i = 0; i < 5; i++) {
            if (names[i].toLowerCase().equals(text.toLowerCase())) {
                index = i;
            }
        }
        return descriptions[index];
    }

    public String[] getConditions(String text) {
        int index = 0;
        for (int i = 0; i < 5; i++) {
            if (names[i].toLowerCase().equals(text.toLowerCase())) {
                index = i;
            }
        }
        return conditions[index];
    }

    public HashMap<String, String> getInteractions(String text) {
        int index = 0;
        for (int i = 0; i < interactions.size(); i++) {
            if (names[i].toLowerCase().equals(text.toLowerCase())) {
                index = i;
            }
        }
        return interactions.get(index);
    }
}
