import net.miginfocom.swing.MigLayout;
import javax.swing.*;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.*;
import java.util.Calendar;

public class Home extends BasePanel {

    public Home(JPanel p, String title)
    {
        super(p, title);
        p.setLayout(new BorderLayout());

        JTextArea tutorialArea = new JTextArea();
        tutorialArea.setLineWrap(true);
        tutorialArea.setWrapStyleWord(true);
        tutorialArea.setEditable(false);
        tutorialArea.setFont(tutorialArea.getFont ().deriveFont (15.3f));
        tutorialArea.setText("Welcome to the Tutorial!\n\n" +
                "***DISCLAIMER***\nAll information posted is merely for educational and informational purposes. " +
                "It is not intended as a substitute for professional advice. Should you choose to act upon any " +
                "information on this application, you do so at your own risk. While the information on this " +
                "application has been verified to the best of our abilities, we cannot guarantee that there are " +
                "no mistakes or errors.\n*********************\n\n" +
                "1) Start the application.\n" +
                "        a) NEW USERS: Enter desired Username and Password. Click 'Sign up'.\n" +
                "        b) RETURNING USERS: Enter pre-existing Username and Password to access account.\n\n" +
                "2) NEW USERS: Enter full name, weight, height, birthday, and sex.\n" +
                "        a) Confirm your new account by logging in with your username and password. \n" +
                "\n" +
                "3) Enter current location for geolocation tracking to nearest hospitals under the EMERGENCY tab.\n" +
                "        a) This is to be used in the instance of an adverse drug event. \n" +
                "\n" +
                "4) Search for drugs you want to add to your account under the WARNINGS tab to make sure that there aren't any dangerous interactions with your other medications.\n" +
                "\n" +
                "5) Add/Remove/Edit drugs from your current regimen under the PROFILE tab using the search engine or the QR code scanner.\n" +
                "        a) Confirm that there are no dangerous interactions between your medications by looking at the drug interactions and the drug description when adding a drug to your account.\n" +
                "\n" +
                "6) Under the CALENDAR tab, create an event and enter the appropriate information to update your medication regimen.\n" +
                "\n" +
                "7) Confirm drugs have been taken at the appropriate time under the CALENDAR tab.\n" +
                "\n" +
                "8) Continue to check CALENDAR daily for reminders of which medications to take and when to take them.\n");
        JScrollPane jp = new JScrollPane(tutorialArea);
        jp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        p.add(jp);
        pack();

        /*
        ClickWindow cw1 = new ClickWindow(p, title);
        cw1.setTextArea("Welcome to the Tutorial!\n\n" +
                "*DISCLAIMER: All information posted is merely for educational and informational purposes. " +
                "It is not intended as a substitute for professional advice. Should you choose to act upon any " +
                "information on this application, you do so at your own risk. While the information on this " +
                "application has been verified to the best of our abilities, we cannot guarantee that there are " +
                "no mistakes or errors.");


        if(ClickWindow.isButtonClicked() == 1)
        {
            ClickWindow cw2 = new ClickWindow(p, title);
            cw2.setTextArea("Page 2");
        }
        */
    }
}



