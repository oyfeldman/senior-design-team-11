import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Warnings extends BasePanel {

    public Warnings(JPanel p, String title) {
        super(p, title);

        p.setLayout(new MigLayout());
        JLabel searchLabel = new JLabel("Drug Name");
        searchLabel.setFont(searchLabel.getFont ().deriveFont (20.0f));
        JTextField searchField = new JTextField();
        searchField.setFont(searchField.getFont ().deriveFont (20.0f));
        searchField.setColumns(30);
        JButton searchButton = new JButton("Search");
        searchButton.setFont(searchButton.getFont ().deriveFont (20.0f));
        JTextArea searchResults = new JTextArea();
        searchResults.setFont(searchResults.getFont ().deriveFont (18.0f));
        searchResults.setForeground(Color.RED);
        searchResults.setRows(10);
        searchResults.setColumns(50);
        searchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (searchField.getText().equals("Ibuprofen")) {
                    searchResults.setText("Amoxicillin\tIbuprofen may decrease the excretion rate of Amoxicillin which could result in a higher serum level.\n" +
                            "Cirazoline\tThe risk or severity of hypertension can be increased when Ibuprofen is combined with Cirazoline.");
                }
                else if (searchField.getText().equals("Phenelzine")) {
                    searchResults.setText("Amoxicillin\tPhenelzine may decrease the excretion rate of Amoxicillin which could result in a higher serum level.\n" +
                            "Cirazoline\tPhenelzine may increase the hypertensive activities of Cirazoline.");
                }
                else {
                    searchResults.setText("There are no drug interactions between your current medications and the searched drug.");
                }
            }
        });

        p.add(searchLabel);
        p.add(searchField);
        p.add(searchButton, "wrap");
        p.add(searchResults, "span");
    }
}
