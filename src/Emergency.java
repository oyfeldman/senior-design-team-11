import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import net.miginfocom.swing.MigLayout;

public class Emergency extends BasePanel {
    public Emergency(JPanel p, String title) {
        super(p, title);
        p.setLayout(new MigLayout());
        JTextField getLocation = new JTextField();
        JLabel locationLabel = new JLabel();
        JButton setLocation = new JButton("Set Location");
        JLabel emergencyInfo1 = new JLabel("In the event of an emergency, call 911 and go to the hospital immediately.");
        emergencyInfo1.setForeground(Color.RED);
        JLabel emergencyInfo2 = new JLabel("Call Poison Control at 1-800-222-1222.");
        emergencyInfo2.setForeground(Color.RED);

        locationLabel.setText("Current Location: ");
        locationLabel.setFont(locationLabel.getFont ().deriveFont (18.0f));
        p.add(locationLabel);
        getLocation.setFont(getLocation.getFont ().deriveFont (18.0f));
        getLocation.setColumns(25);
        p.add(getLocation);
        setLocation.setFont(setLocation.getFont ().deriveFont (18.0f));
        setLocation.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    p.removeAll();
                    locationLabel.setText("Current Location: ");
                    locationLabel.setFont(locationLabel.getFont ().deriveFont (18.0f));
                    p.add(locationLabel);
                    getLocation.setFont(getLocation.getFont ().deriveFont (18.0f));
                    getLocation.setColumns(25);
                    p.add(getLocation);
                    setLocation.setFont(setLocation.getFont ().deriveFont (18.0f));
                    p.add(setLocation, "wrap");
                    p.add(emergencyInfo1, "span, grow, wrap");
                    emergencyInfo1.setFont(emergencyInfo1.getFont ().deriveFont (18.0f));
                    p.add(emergencyInfo2, "span, grow, wrap");
                    emergencyInfo2.setFont(emergencyInfo2.getFont ().deriveFont (18.0f));

                    String currentLocation = getLocation.getText().replace(" ", "");
                    String imageUrl = "https://maps.googleapis.com/maps/api/staticmap?center=" + currentLocation + "&zoom=13&size=612x612&scale=2&maptype=roadmap&markers=color:red|size:small|41.661621,-91.549919|41.663891,-91.528969|41.681980,-91.563680&key=AIzaSyBAFiRRvmePE-2evv8shexVTr8682cXgnw";
                    String destinationFile = "image.jpg";
                    URL url = new URL(imageUrl);
                    InputStream is = url.openStream();
                    OutputStream os = new FileOutputStream(destinationFile);

                    byte[] b = new byte[2048];
                    int length;

                    while ((length = is.read(b)) != -1) {
                        os.write(b, 0, length);
                    }

                    is.close();
                    os.close();
                } catch (IOException e2) {
                    e2.printStackTrace();
                    System.exit(1);
                }

                JLabel map = new JLabel(new ImageIcon((new ImageIcon("image.jpg")).getImage().getScaledInstance(630, 600,
                        java.awt.Image.SCALE_SMOOTH)));

                p.add(map, "span");

                p.revalidate();
            }
        });
        p.add(setLocation, "wrap");
        p.add(emergencyInfo1, "span, grow, wrap");
        emergencyInfo1.setFont(emergencyInfo1.getFont ().deriveFont (18.0f));
        p.add(emergencyInfo2, "span, grow, wrap");
        emergencyInfo2.setFont(emergencyInfo2.getFont ().deriveFont (18.0f));

        p.setVisible(true);
    }
}
